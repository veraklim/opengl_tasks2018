/**
Использование текстуры в качестве коэффициента отражения света
*/

#version 330

struct LightInfo
{
	vec3 pos; //положение источника света в системе координат ВИРТУАЛЬНОЙ КАМЕРЫ!
	vec3 La; //цвет и интенсивность окружающего света
	vec3 Ld; //цвет и интенсивность диффузного света
	vec3 Ls; //цвет и интенсивность бликового света
};

uniform sampler2D diffuseTex;
uniform LightInfo light[3];

in vec3 vertexNormalRespectToCamera; //нормаль в системе координат камеры (интерполирована между вершинами треугольника)
in vec4 vertexPositionRespectToCamera; //координаты вершины в системе координат камеры (интерполированы между вершинами треугольника)
in vec2 texCoord; //текстурные координаты (интерполирована между вершинами треугольника)

out vec4 fragColor; //выходной цвет фрагмента

const vec3 Ks = vec3(1.0, 1.0, 1.0); //Коэффициент бликового отражения
const float shininess = 128.0;
const float inner_cutoff = radians(12.5f);
const float outer_cutoff = radians(17.5f);

void main()
{
    vec3 viewDirection = normalize(-vertexPositionRespectToCamera.xyz);
    vec3 normal = normalize(vertexNormalRespectToCamera);;
    float VdotN = dot(viewDirection, vertexNormalRespectToCamera);

	if (VdotN < 0.0) {
		normal = -normalize(vertexNormalRespectToCamera);
    }

	vec3 diffuseColor = texture(diffuseTex, texCoord).rgb;
    vec3 color = vec3(0.0);

    vec3 glareLight = vec3(0.0);
    vec3 regularLight = vec3(0.0);

    for (int i = 0; i < 3; i++) {
        vec3 lightDirCamSpace = normalize(light[i].pos - vertexPositionRespectToCamera.xyz); //направление на источник света
        float NdotL = max(dot(normal, lightDirCamSpace.xyz), 0.0); //скалярное произведение (косинус)
        float distance = length(light[i].pos - vertexPositionRespectToCamera.xyz);
        float attenuationCoef = 2.0 / (1.0 + 2.0 * distance);

        if (i != 2) {
            regularLight += attenuationCoef * (light[i].La + light[i].Ld * NdotL);
			
			if (NdotL > 0.0) {
				vec3 halfVector = normalize(lightDirCamSpace.xyz + viewDirection); //биссектриса между направлениями на камеру и на источник света

		        float blinnTerm = max(dot(normal, halfVector), 0.0); //интенсивность бликового освещения по Блинну
		        blinnTerm = pow(blinnTerm, shininess); //регулируем размер блика
		        glareLight += attenuationCoef*light[i].Ls * Ks * blinnTerm;
			}	
        }
        else {
            NdotL = max(dot(normal, normalize(-vertexPositionRespectToCamera.xyz)), 0.0);
            distance = length(-vertexPositionRespectToCamera.xyz);
            float _cos = -dot(vec3(0, 0, -1), viewDirection);
            regularLight += max(attenuationCoef * min((light[i].La + light[i].Ld * NdotL) * (- 1 - ((acos(_cos) - outer_cutoff) / (acos(_cos)))),
                                            2.5 * (light[i].La + light[i].Ld * NdotL)), 0.0);
			if (_cos > cos(inner_cutoff)) {
				vec3 halfVector = normalize(lightDirCamSpace.xyz + viewDirection); //биссектриса между направлениями на камеру и на источник света

		        float blinnTerm = max(dot(normal, halfVector), 0.0); //интенсивность бликового освещения по Блинну
		        blinnTerm = pow(blinnTerm, shininess); //регулируем размер блика
				glareLight += max(attenuationCoef * min(light[i].Ls * Ks * blinnTerm * (- 1 - ((acos(_cos) - inner_cutoff) / (acos(_cos)))),
                                            2.5 * light[i].Ls * Ks * blinnTerm), 0.0);
				break;
			}
        }
	}

    color = regularLight * diffuseColor + glareLight;
	fragColor = vec4(color, 1.0);
}
