#version 330

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;
uniform mat3 normalToCameraMatrix;

layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec3 vertexNormal;
layout(location = 2) in vec2 vertexTexCoord;

out vec3 vertexNormalRespectToCamera; //нормаль в системе координат камеры
out vec4 vertexPositionRespectToCamera; //координаты вершины в системе координат камеры
out vec2 texCoord;

void main()
{
    vertexPositionRespectToCamera = viewMatrix * modelMatrix * vec4(vertexPosition, 1.0);
    vertexNormalRespectToCamera = normalize(normalToCameraMatrix * vertexNormal);

    gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(vertexPosition, 1.0);

	texCoord = vertexTexCoord;
}
