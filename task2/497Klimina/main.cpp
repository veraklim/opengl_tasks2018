#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>
#include <Texture.hpp>
#include <LightInfo.hpp>

#include <iostream>
#include <vector>
#include <algorithm>
#include <sstream>


const unsigned int LightNum = 3;


glm::vec3 countNormal(float a, float u, float v) {
    glm::vec3 dev_u = glm::vec3(
        sin(u) * (sin(u / 2) * sin(2 * v) - cos(u / 2) * sin(v) - a) - cos(u) * (sin(u / 2) * sin(v) + cos(u / 2) * sin(2 * v)) / 2,
        cos(u) * (a + cos(u / 2) * sin(v) - sin(u / 2) * sin(2 * v)) - sin(u) * (sin(u / 2) * sin(v) + cos(u / 2) * sin(2 * v)) / 2,
        (cos(u / 2) * sin(v) - sin(u / 2) * sin(2 * v)) / 2);
    glm::vec3 dev_v = glm::vec3(
        cos(u) * (cos(u / 2) * cos(v) - 2 * sin(u / 2) * cos(2 * v)),
        sin(u) * (cos(u / 2) * cos(v) - 2 * sin(u / 2) * cos(2 * v)),
        2 * cos(u / 2) * cos(2 * v) + sin(u / 2) * cos(v));
    return glm::normalize(glm::cross(dev_u, dev_v));
}

MeshPtr makeKleinBottle(float a, float period) {
    
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> texcoords;
    
    for (unsigned int i = 0; i < period; i++)
    {
        float u = 2 * glm::pi<float>() * i / period;
        float u1 = (i + 1) > period ? 2 * glm::pi<float>() : 2 * glm::pi<float>() * (i + 1) / period;
        
        for (unsigned int j = 0; j < period; j++)
        {
            float v = 2 * glm::pi<float>() * j / period;
            float v1 = (j + 1) > period ? 2 * glm::pi<float>() : 2 * glm::pi<float>() * (j + 1) / period;

            glm::vec3 vertex_u_v = glm::vec3(
                (a + cos(u / 2) * sin(v) - sin(u / 2) * sin(2 * v)) * cos(u), 
                (a + cos(u / 2) * sin(v) - sin(u / 2) * sin(2 * v)) * sin(u), 
                sin(u / 2) * sin(v) + cos(u / 2) * sin(2 * v));
            glm::vec3 vertex_u_v1 = glm::vec3(
                (a + cos(u / 2) * sin(v1) - sin(u / 2) * sin(2 * v1)) * cos(u), 
                (a + cos(u / 2) * sin(v1) - sin(u / 2) * sin(2 * v1)) * sin(u), 
                sin(u / 2) * sin(v1) + cos(u / 2) * sin(2 * v1));
            glm::vec3 vertex_u1_v = glm::vec3(
                (a + cos(u1 / 2) * sin(v) - sin(u1 / 2) * sin(2 * v)) * cos(u1), 
                (a + cos(u1 / 2) * sin(v) - sin(u1 / 2) * sin(2 * v)) * sin(u1), 
                sin(u1 / 2) * sin(v) + cos(u1 / 2) * sin(2 * v));
            glm::vec3 vertex_u1_v1 = glm::vec3(
                (a + cos(u1 / 2) * sin(v1) - sin(u1 / 2) * sin(2 * v1)) * cos(u1), 
                (a + cos(u1 / 2) * sin(v1) - sin(u1 / 2) * sin(2 * v1)) * sin(u1), 
                sin(u1 / 2) * sin(v1) + cos(u1 / 2) * sin(2 * v1));

            vertices.push_back(vertex_u_v);
            vertices.push_back(vertex_u_v1);
            vertices.push_back(vertex_u1_v);
            vertices.push_back(vertex_u_v1);
            vertices.push_back(vertex_u1_v);
            vertices.push_back(vertex_u1_v1);

            normals.push_back(countNormal(a, u, v));
            normals.push_back(countNormal(a, u, v1));
            normals.push_back(countNormal(a, u1, v));
            normals.push_back(countNormal(a, u, v1));
            normals.push_back(countNormal(a, u1, v));
            normals.push_back(countNormal(a, u1, v1));

            texcoords.push_back(glm::vec2(u, v));
            texcoords.push_back(glm::vec2(u, v1));
            texcoords.push_back(glm::vec2(u1, v));
            texcoords.push_back(glm::vec2(u, v1));
            texcoords.push_back(glm::vec2(u1, v));
            texcoords.push_back(glm::vec2(u1, v1));
        }
    }
    
    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());
    
    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());
    
    return mesh;
}

class SampleApplication : public Application
{
public:

    MeshPtr _KleinBottle;
    // маркер для источника света
    MeshPtr _marker;

    ShaderProgramPtr _shader;
    ShaderProgramPtr _markerShader;

    float _period   = 50.0f;
    float a         = 3.0f;

    // координаты источников света
    float _lr0      = 3.5f;
    float _phi0     = 0.0f;
    float _theta0   = glm::pi<float>() * 0.2f;

    float _lr1      = 10.0f;
    float _phi1     = 0.0f;
    float _theta1   = glm::pi<float>() * 0.25f;

    LightInfo _light[LightNum];

    TexturePtr _worldTexture;

    GLuint _sampler;


    void makeScene() override
    {
        Application::makeScene();

        _cameraMover    = std::make_shared<FreeCameraMover>();

        _KleinBottle    = makeKleinBottle(a, _period);
        _marker         = makeSphere(0.1f);

        _shader         = std::make_shared<ShaderProgram>("497KliminaData/texture.vert", "497KliminaData/texture.frag");
        _markerShader   = std::make_shared<ShaderProgram>("497KliminaData/marker.vert", "497KliminaData/marker.frag");

        _light[0].position  = glm::vec3(glm::cos(_phi0) * glm::cos(_theta0), glm::sin(_phi1) * glm::cos(_theta0), glm::sin(_theta0)) * _lr0;
        _light[0].ambient   = glm::vec3(0.0, 0.0, 0.0);
        _light[0].diffuse   = glm::vec3(0.4, 0.4, 0.4);
        _light[0].specular  = glm::vec3(1.0, 1.0, 1.0);

        _light[1].position  = glm::vec3(glm::cos(_phi1) * glm::cos(_theta1), glm::sin(_phi1) * glm::cos(_theta1), glm::sin(_theta1)) * _lr1;
        _light[1].ambient   = glm::vec3(0.0, 0.0, 0.0);
        _light[1].diffuse   = glm::vec3(0.8, 0.0, 0.0);
        _light[1].specular  = glm::vec3(0.8, 0.0, 0.0);

        _light[2].position  = glm::vec3(0.0, 0.0, 5.0);
        _light[2].ambient   = glm::vec3(0.0, 0.0, 0.0);
        _light[2].diffuse   = glm::vec3(0.0, 0.8, 0.0);
        _light[2].specular  = glm::vec3(0.0, 0.8, 0.0);

        _worldTexture = loadTexture("497KliminaData/steklovol.jpg");

        glGenSamplers(1, &_sampler);
        glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);
    }

    void update() override
    {
        float dt = glfwGetTime() - _oldTime;

        if (glfwGetKey(_window, GLFW_KEY_MINUS) == GLFW_PRESS) {
            _period = std::max(4.0f, _period - 100 * dt);
            std::cout << _period << std::endl;
            _KleinBottle = makeKleinBottle(a, _period);
        }
        if (glfwGetKey(_window, GLFW_KEY_EQUAL) == GLFW_PRESS) {
            _period = std::min(100.0f, _period + 100 * dt);
            std::cout << _period << std::endl;
            _KleinBottle = makeKleinBottle(a, _period);
        }
        Application::update();
    }

    void draw() override
    {
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        _shader->use();
        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        _phi0 += 0.001;
        if (_phi0 >= 2 * glm::pi<float>()) {
            _phi0 -= 2 * glm::pi<float>();
        }

        _light[0].position = glm::vec3(glm::cos(_phi0) * glm::cos(_theta0), glm::sin(_phi0) * glm::cos(_theta0), glm::sin(_theta0)) * _lr0;

        std::ostringstream str;
        glm::vec3 lightPosCamSpace;

        for (unsigned int i = 0; i < LightNum; i++)
        {
            str << "light[" << i << ']';

            lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_light[i].position, 1.0f));

            _shader->setVec3Uniform(str.str() + ".pos", lightPosCamSpace); //копируем положение уже в системе виртуальной камеры
            _shader->setVec3Uniform(str.str() + ".La", _light[i].ambient);
            _shader->setVec3Uniform(str.str() + ".Ld", _light[i].diffuse);
            _shader->setVec3Uniform(str.str() + ".Ls", _light[i].specular);
            str.str("");
        }


        glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0
        glBindSampler(0, _sampler);
        _worldTexture->bind();

        _shader->setIntUniform("diffuseTex", 0);
        _shader->setMat4Uniform("modelMatrix", _KleinBottle->modelMatrix());
        _shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _KleinBottle->modelMatrix()))));

        // рисуем меш
        _KleinBottle->draw();

        for (unsigned int i = 0; i < LightNum - 1; i++)
        {
            _markerShader->use();
            _markerShader->setMat4Uniform("mvpMatrix", _camera.projMatrix * _camera.viewMatrix * glm::translate(glm::mat4(1.0f), _light[i].position));
            _markerShader->setVec4Uniform("color", glm::vec4(_light[i].diffuse, 1.0f));
            _marker->draw();
        }

        // отсоединяем сэмплер и шейдерную программу
        glBindSampler(0, 0);
        glUseProgram(0);
    }
};

int main()
{
    SampleApplication app;
    app.start();

    return 0;
}